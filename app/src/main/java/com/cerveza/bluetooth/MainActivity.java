package com.cerveza.bluetooth;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.drawer.WearableDrawerLayout;
import android.support.wearable.view.drawer.WearableNavigationDrawer;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends Activity implements IOnNavigationChange{

    private WearableDrawerLayout mWearableDrawerLayout;
    private WearableNavigationDrawer mWearableNavigationDrawer;

    BluetoothAdapterFragment bluetoothAdapterFragment;
    BluetoothDiscoveredFragment bluetoothDiscoveredFragment;
    BluetoothConnectedFragment bluetoothConnectedFragment;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();

        bluetoothAdapterFragment = new BluetoothAdapterFragment();
        bluetoothDiscoveredFragment =  new BluetoothDiscoveredFragment();
        bluetoothConnectedFragment = new BluetoothConnectedFragment();

        // Main Wearable Drawer Layout that wraps all content
        mWearableDrawerLayout = (WearableDrawerLayout) findViewById(R.id.drawer_layout);
        mWearableDrawerLayout.peekDrawer(Gravity.TOP);

        // Top Navigation Drawer
        mWearableNavigationDrawer =
                (WearableNavigationDrawer) findViewById(R.id.top_navigation_drawer);
        NavigationAdapter navigationAdapter = new NavigationAdapter(this);
        mWearableNavigationDrawer.setAdapter(navigationAdapter);

        navigationAdapter.onItemSelected(1);
        mWearableNavigationDrawer.setCurrentItem(1,false);

    }

    public void onNavigationChange(int pos) {
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = bluetoothAdapterFragment; // fragmentToggles;

        switch (pos){
            case 0:
                fragment = bluetoothConnectedFragment;
                break; //overview
            case 1:
                fragment = bluetoothAdapterFragment;
                break;
            case 2:
                fragment = bluetoothDiscoveredFragment;
                break; //toggles
        }

        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }
}
