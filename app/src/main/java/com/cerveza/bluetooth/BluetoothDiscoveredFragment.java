package com.cerveza.bluetooth;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cerveza on 18.03.2017.
 */

public class BluetoothDiscoveredFragment extends Fragment {
    AdapterBluetoothList listDiscoveredAdapter;
    Set<BluetoothDevice> discoveredDevices;
    Context context;

    BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothReciever;
    IntentFilter filter;

    public void onAttach(Context context) {
        this.context = context;
        /*this.context = context;
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        context = activity.getApplicationContext();
       /* this.context = activity.getApplicationContext();
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        discoveredDevices = new HashSet<>();

        listDiscoveredAdapter = new AdapterBluetoothList(context, discoveredDevices);

        setFilter();
        createBroadcastReceiver();
        super.onCreate(savedInstanceState);
    }

    private void setFilter(){
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!bluetoothAdapter.isEnabled())
            bluetoothAdapter.enable();

        registerReceiver();

        bluetoothAdapter.startDiscovery();

    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterReceiver();

        bluetoothAdapter.cancelDiscovery();
    }

    private void createBroadcastReceiver(){
        bluetoothReciever = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothDevice device;
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    discoveredDevices.add(device);
                    listDiscoveredAdapter.refresh(discoveredDevices);
                }

            }
        };
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bluetooth_discovered_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.discovered_devices_list);
        recyclerView.setAdapter(listDiscoveredAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setNestedScrollingEnabled(false);
        return  view;
    }

    private void registerReceiver(){

        context.registerReceiver(bluetoothReciever,filter);
    }

    private void unregisterReceiver(){
        context.unregisterReceiver(bluetoothReciever);
    }
}
