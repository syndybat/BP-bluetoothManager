package com.cerveza.bluetooth;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cerveza on 18.03.2017.
 */

public class BluetoothAdapterFragment extends Fragment{
    Context context;
    BluetoothAdapter bluetoothAdapter;

    AdapterBluetoothList listBondedAdapter;
    Set<BluetoothDevice> bondedDevices;

    TextView textName;
    TextView textAddress;

    public void onAttach(Context context) {
        this.context = context;
        /*this.context = context;
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        context = activity.getApplicationContext();
       /* this.context = activity.getApplicationContext();
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bondedDevices = bluetoothAdapter.getBondedDevices();

        listBondedAdapter = new AdapterBluetoothList(context, bondedDevices);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        textName.setText(bluetoothAdapter.getName());
        textAddress.setText(bluetoothAdapter.getAddress());

        listBondedAdapter.refresh(bondedDevices);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bluetooth_adapter_fragment, container, false);
        textName = (TextView)view.findViewById(R.id.adapter_name);
        textAddress = (TextView)view.findViewById(R.id.adapter_address);
        RecyclerView recyclerView;

        recyclerView = (RecyclerView) view.findViewById(R.id.bonded_devices_list);
        recyclerView.setAdapter(listBondedAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setNestedScrollingEnabled(false);
        return  view;
    }
}
