package com.cerveza.bluetooth;

/**
 * Created by cerveza on 23/02/2017.
 */

public interface IOnNavigationChange {
        void onNavigationChange(int pos);
}
