package com.cerveza.bluetooth;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cerveza on 18.03.2017.
 */

public class BluetoothConnectedFragment extends Fragment {

    Context context;
    BluetoothAdapter bluetoothAdapter;
    BluetoothManager bluetoothManager;
    private BroadcastReceiver bluetoothReciever;
    IntentFilter filter;

    AdapterBluetoothList listConnectedAdapter;
    Set<BluetoothDevice> connectedDevices;

    public void onAttach(Context context) {
        this.context = context;
        /*this.context = context;
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        context = activity.getApplicationContext();
       /* this.context = activity.getApplicationContext();
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        connectedDevices = new HashSet<>();

        listConnectedAdapter = new AdapterBluetoothList(context, connectedDevices);

        setFilter();
        createBroadcastReceiver();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!bluetoothAdapter.isEnabled())
            bluetoothAdapter.enable();

        registerReceiver();


    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterReceiver();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bluetooth_connected_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.connected_devices_list);
        recyclerView.setAdapter(listConnectedAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setNestedScrollingEnabled(false);
        return  view;
    }


    private void setFilter(){
        filter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
    }

    private void createBroadcastReceiver(){
        bluetoothReciever = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothDevice device;

                if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                    connectedDevices.remove(intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE));
                    listConnectedAdapter.refresh(connectedDevices);
                }else if(BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)){
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    connectedDevices.add(device);
                    listConnectedAdapter.refresh(connectedDevices);
                }

            }
        };
    }

    private void registerReceiver(){
        Intent intent = context.registerReceiver(bluetoothReciever,filter);
    }

    private void unregisterReceiver(){
        context.unregisterReceiver(bluetoothReciever);
    }
}
