package com.cerveza.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Set;

/**
 * Created by cerveza on 08.03.2017.
 */

public class AdapterBluetoothList extends RecyclerView.Adapter {
    private BluetoothDevice[] mDataset;
    private final Context mContext;
    private final LayoutInflater mInflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterBluetoothList(Context context, Set<BluetoothDevice> dataset) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDataset =  dataset.toArray(new BluetoothDevice[dataset.size()]);

    }


    public void refresh(Set<BluetoothDevice> dataset)
    {
        this.mDataset = dataset.toArray(new BluetoothDevice[dataset.size()]);
        notifyDataSetChanged();
    }


    // Provide a reference to the type of views you're using
    private class ItemViewHolder extends RecyclerView.ViewHolder
    {
        public TextView macAddress;
        public TextView name;

        public ItemViewHolder(View itemView) {
            super(itemView);
            macAddress = (TextView) itemView.findViewById(R.id.text_mac_address);
            name = (TextView) itemView.findViewById(R.id.text_name);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(mInflater.inflate(R.layout.bluetooth_list_item, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // retrieve the text view
        ItemViewHolder itemHolder = (ItemViewHolder) holder;

        itemHolder.name.setText(mDataset[position].getName());
        itemHolder.macAddress.setText(mDataset[position].getAddress());

        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
