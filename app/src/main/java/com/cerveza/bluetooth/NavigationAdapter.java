package com.cerveza.bluetooth;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.wearable.view.drawer.WearableNavigationDrawer;

import java.util.ArrayList;

/**
 * Created by cerveza on 11/02/2017.
 */

public class NavigationAdapter
        extends WearableNavigationDrawer.WearableNavigationDrawerAdapter{

    private Context mContext;
    private IOnNavigationChange onNavigationChangeListener = null;
    private ArrayList<String> items;


    public NavigationAdapter(Context context) {
        mContext = context;
        if(context instanceof IOnNavigationChange){
            onNavigationChangeListener = (IOnNavigationChange) context;
        }

        items = new ArrayList<>();
        items.add("Connected");
        items.add("Adapter");
        items.add("Discovered");

    }

    @Override
    public int getCount() {
        return items.size();
        //return items.size();
    }

    @Override
    public void onItemSelected(int position) {
        onNavigationChangeListener.onNavigationChange(position);
        return;
    }

    @Override
    public String getItemText(int pos) {
        return items.get(pos);
    }

    @Override
    public Drawable getItemDrawable(int pos) {
        String imageName = "ic_nav_".concat(items.get(pos).toLowerCase());
        int resID = mContext.getResources().getIdentifier(imageName , "drawable", mContext.getPackageName());
        return mContext.getDrawable(resID);

    }

}
